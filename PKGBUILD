# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Johansen
# Contributor: Caleb Fontenot <foley2431 at gmail dot com>

pkgname=mmsd-tng
pkgver=2.6.3
pkgrel=1
pkgdesc="Multimedia Messaging Service Daemon - The Next Generation"
arch=('x86_64' 'aarch64')
url="https://gitlab.com/kop316/mmsd"
license=('GPL-2.0-or-later')
depends=(
  'c-ares'
  'libmm-glib'
  'libphonenumber'
  'libsoup3'
  'mobile-broadband-provider-info'
)
makedepends=(
  'git'
  'meson'
)
provides=('mmsd')
conflicts=('mmsd')
source=("git+https://gitlab.com/kop316/mmsd.git#tag=$pkgver"
        "$pkgname.service")
sha256sums=('6d7b5a80bb8475bf9556923aba29301b752596215cad08237c104e15073f3c6c'
            '922f43ec982820e9614be314214fbe9526d92d59063960a8173c2333b59b36fd')

prepare() {
  cd mmsd

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  arch-meson mmsd build -Dbuild-mmsctl=true
  meson compile -C build
}

check() {
  meson test -C build --no-rebuild --print-errorlogs
}

package() {
  meson install -C build --no-rebuild --destdir "$pkgdir"

  install -d "$pkgdir"/usr/lib/systemd/user/default.target.wants
  install -Dm644 ../"$pkgname.service" "$pkgdir"/usr/lib/systemd/user/
  ln -s "/usr/lib/systemd/user/$pkgname.service" \
    "$pkgdir/usr/lib/systemd/user/default.target.wants/$pkgname.service"

  install -Dm755 build/tools/{create-hex-array,decode-mms} -t "$pkgdir"/usr/bin/
}
